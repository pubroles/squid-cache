import os
import pytest
import re

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_package(host):
    p = host.package('squid')
    assert p.is_installed


def test_service(host):
    service = host.service("squid")
    assert service.is_running
    assert service.is_enabled


@pytest.mark.parametrize('directory', [
    '/home/spool/squid',
    '/home/spool/squid/00'
])
def test_pkg(host, directory):
    d = host.file(directory)
    assert d.exists
    assert d.is_directory


def test_cache(host):
    def get_time(stderr):
        t = [i for i in stderr.splitlines() if 'real' in i][0]
        z = re.compile(r'real\s(\d+)m(\d+)')
        b = z.search(t).groups()
        return int(b[0])*60 + int(b[1])

    url = "http://speed.hetzner.de/100MB.bin"
    r1 = host.run("time http_proxy=http://127.0.0.1:3128 curl -o big_file %s" %
                  url)
    assert r1.rc == 0
    time1 = get_time(r1.stderr)
    r2 = host.run("time http_proxy=http://127.0.0.1:3128 curl -o big_file %s" %
                  url)
    assert r2.rc == 0
    time2 = get_time(r2.stderr)
    assert time2 < time1/2
